<!--
 * @Author: e4glet
 * @Date: 2020-05-10 21:06:31
 * @LastEditTime: 2020-05-10 21:47:15
 * @LastEditors: Please set LastEditors
 * @Description: In User Settings Edit
 * @FilePath: \kubernetes\gmssl\gmssl.md
 -->


# 使用GMSSL工具生成自签证书工具（Windows版）

支持Win10  

#### 配置方法：  
首先复制openssl.cnf到C:\Program Files (x86)\Common Files\SSL文件夹，没有的话就创建这个文件夹  

使用CMD（管理员模式运行）  

#### 使用方法

生成SM2密钥并签名  

```c
#生成密钥对
G:\gmssl>gmssl genpkey -algorithm EC -pkeyopt ec_paramgen_curve:sm2p256v1 -pkeyopt ec_param_enc:named_curve -out ca-key.pem

#生成CA证书
G:\gmssl>gmssl req -new -x509 -key ca-key.pem -out ca.pem
You are about to be asked to enter information that will be incorporated
into your certificate request.
What you are about to enter is what is called a Distinguished Name or a DN.
There are quite a few fields but you can leave some blank
For some fields there will be a default value,
If you enter '.', the field will be left blank.
-----
Country Name (2 letter code) [CN]:CN
State or Province Name (full name) [Some-State]:China
Locality Name (eg, city) []:Tianjin
Organization Name (eg, company) [Internet Widgits Pty Ltd]:Tianjin
Organizational Unit Name (eg, section) []:k8s
Common Name (e.g. server FQDN or YOUR name) []:e4glet
Email Address []:xxxxx@qq.com

```

