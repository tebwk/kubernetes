<!--
 * @Author: e4glet
 * @Date: 2020-04-18 16:13:16
 * @LastEditTime: 2020-05-30 19:13:19
 * @LastEditors: Please set LastEditors
 * @Description: In User Settings Edit
 * @FilePath: \kubernetes\README.md
 -->
# kubernetes相关安全资料
<img src="https://img.shields.io/badge/kubernetes-1.18.2-brightgreen.svg" alt="kubernetes">
<img src="https://img.shields.io/badge/KataContainers-1.18.0-brightgreen.svg" alt="Kata Containers">
<img src="https://img.shields.io/badge/rancher-2.x-brightgreen.svg" alt="rancher">
<img src="https://img.shields.io/badge/dashboard-brightgreen.svg" alt="dashboard">
<img src="https://img.shields.io/badge/kuboard-brightgreen.svg" alt="kuboard">

### 作者：e4glet liyaohua8468@gmail.com



#### 一、简介
  kubernetes部署文档及配置文件，安全配置资料等  

  该资料部署kubernetes时版本为v1.18.1，部署后其会自动更新到最新版v1.18.2，读者不用担心安装过程中小版本之间的细微差别。

  后续的相关配置文档也是基于此环境（v1.18.2）进行的。


#### 二、概念知识学习

https://www.qikqiak.com/k8strain/basic/install/

#### 三、目录结构说明

  
	-dashboard  仪表板（包含官方dashboard和kuboard）
	-cfssl  自签证书说明及一键脚本        
	-docs  资料文档
	-flannel  k8s网络配置文件
	-k8s_images_v1.18.1  部署k8s所需的离线镜像文件
	-nginx-ingress ingress配置文件
  


#### 四、部署环境及软件版本
  1. CentOS 7.7
  2. kubernetes v1.18.2
  3. docker-ce v19.3.8


#### 五、视频教程演示及资料

1. 网络公开课之kubernetes集群部署

https://www.bilibili.com/video/BV1Q5411t79v


更正文档中的两处错误：  
1.初始化虚拟机使用NAT模式（与主机共享网络）  
2.文档中配置K8s网络使用的是calico，非flannel 


2. Kubernetes部署dashboard  
文档已完成  
[查看说明](https://gitee.com/e4glet/kubernetes/blob/master/dashboard/dashboard.md)


3. Kubernetes部署Kuboard  
文档已完成  
[查看说明](https://gitee.com/e4glet/kubernetes/blob/master/dashboard/kuboard.md)


4. 应用Kata提升Kubernetes安全性  
文档已完成  
请查阅 [Kata containers应用Kubernetes部署](https://gitee.com/e4glet/kubernetes/blob/master/docs/Kata%20containers应用Kubernetes部署.pdf)



5. Security Context of Kubernetes  
资料整理完成  
请查阅 [Security Context of Kubernetes](https://gitee.com/e4glet/kubernetes/blob/master/docs/Security%20Context%20of%20Kubernetes.pdf)
  
6. 使用cfssl自签证书用于Kubernetes  
请查阅 [说明文档](https://gitee.com/e4glet/kubernetes/blob/master/cfssl/README.md)

7. 使用GMSSL工具生成自签证书工具  
请查阅 [说明文档](https://gitee.com/e4glet/kubernetes/blob/master/gmssl/README.md)


8. loopfs磁盘加密  
[查看说明](https://gitee.com/e4glet/kubernetes/blob/master/NFS/README.md)


#### 友情提示

文档整理来自本作者，作者的初衷是完善kubernetes的部署教程，一切以学习为目的，禁止任何人将本作者的教程倒卖获取利益，如用户发现有人倒卖资料，请拒绝付款！

为了尽早的分享我们的研究成果，可能内容上会有一些文字描述错误，敬请见谅，我们会在发现错误后及时更正。