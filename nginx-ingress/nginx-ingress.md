<!--
 * @Author: e4glet
 * @Date: 2020-05-09 14:27:59
 * @LastEditTime: 2020-05-10 11:45:34
 * @LastEditors: Please set LastEditors
 * @Description: In User Settings Edit
 * @FilePath: \kubernetes\nginx-ingress\nginx-ingress.md
 -->

# NGINX Ingress Controller

在 master 节点上执行
```c
kubectl apply -f https://kuboard.cn/install-script/v1.18.x/nginx-ingress.yaml
```

运行完命令后需要稍等一会