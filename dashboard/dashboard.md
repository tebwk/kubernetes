<!--
 * @Author: e4glet
 * @Date: 2020-05-09 23:07:50
 * @LastEditTime: 2020-05-09 23:22:22
 * @LastEditors: Please set LastEditors
 * @Description: In User Settings Edit
 * @FilePath: \kubernetes\dashboard\dashboard.md
 -->

# dashboard

#### 部署仪表板

```c
kubectl apply -f recommended.yaml
```

查看仪表板运行状态
```c
kubectl get svc -n kubernetes-dashboard
```

#### 新建一个管理员用户
在master命令界面  
新建一个manager.yaml  
管理用户名为：e4glet  

```c
kubectl apply -f manager.yaml
```

查看sa和secret
```c
kubectl get sa,secrets -n kubernetes-dashboard
```

具体操作步骤请[查阅文档](https://gitee.com/e4glet/kubernetes/blob/master/docs/Kubernetes%E9%83%A8%E7%BD%B2dashboard%E5%AE%89%E8%A3%85%E6%96%87%E6%A1%A3.pdf)
