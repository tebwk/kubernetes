<!--
 * @Author: e4glet
 * @Date: 2020-05-09 14:17:26
 * @LastEditTime: 2020-05-09 23:22:29
 * @LastEditors: Please set LastEditors
 * @Description: In User Settings Edit
 * @FilePath: \kubernetes\dashboard\kuboard.md
 -->

# kuboard面板镜像

version：v1.09.6


#### 镜像获取

百度网盘：  
链接：https://pan.baidu.com/s/1NAMOf474hokOsK36q_OKcQ   
提取码：t9gz   

#### 加载方法

将镜像包上传至服务器指定目录  

导入镜像：  
```c
docker load < kuboard.tar
```
 
为镜像重新添加标签：  

```c
docker tag 0146965e6475 eipwork/kuboard:latest
```

#### 安装kuboard

```c
kubectl apply -f kuboard.yaml
```

详细操作步骤请[查阅文档](https://gitee.com/e4glet/kubernetes/blob/master/docs/Kubernetes%E5%AE%89%E8%A3%85Kuboard%E7%AE%A1%E7%90%86%E9%9D%A2%E6%9D%BF.pdf)