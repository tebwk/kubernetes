<!--
 * @Author: your name
 * @Date: 2020-06-30 10:40:32
 * @LastEditTime: 2020-06-30 10:43:20
 * @LastEditors: Please set LastEditors
 * @Description: In User Settings Edit
 * @FilePath: \kubernetes\rancher\readme.md
--> 



# 部署rancher管理Kubernetes

#### 安装部署rancher

```c
sudo docker run -d --restart=unless-stopped -p 80:80 -p 443:443 rancher/rancher
```

#### 使用方法

```c
打开浏览器，输入主机的 IP 地址：https://<SERVER_IP>
请使用真实的主机 IP 地址替换 <SERVER_IP>
```
