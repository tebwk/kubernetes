<!--
 * @Author: your e4glet
 * @Date: 2020-05-25 19:15:30
 * @LastEditTime: 2020-06-19 14:40:40
 * @LastEditors: Please set LastEditors
 * @Description: In User Settings Edit
 * @FilePath: \kubernetes\autoinstall\README.md
--> 


# Kubernetes 自动化安装

## 介绍
为了方便新人快速上手和使用，自动化部署脚本分为两部分

第一部分：单master自动化部署脚本
第二部分：多master自动化部署脚本


## 第一部分（作者：e4glet）

#### 安装master节点

将脚本文件init_master.sh上传到管理节点服务器上，根据需要修改脚本前几行的主机名和节点服务器的IP地址  

然后运行脚本  
```c
chmod +x init_master.sh
./init_master.sh
```

这里考虑一些个性化需求，未全部自动化

可以在初始化master之前提前下载好所需镜像文件  

使用如下命令提前下载好集群所需镜像  
```c
kubeadm config images pull
```

初始化master
```c
kubeadm init --apiserver-advertise-address=192.168.202.128 --pod-network-cidr=10.244.0.0/16 --kubernetes-version=v1.18.2
```

以上参数根据需要修改即可，如无特殊需求，直接运行。  

按要求配置  
```c
mkdir -p $HOME/.kube
sudo cp -i /etc/kubernetes/admin.conf $HOME/.kube/config
sudo chown $(id -u):$(id -g) $HOME/.kube/config
```

安装calico网络
```c
kubectl apply -f https://docs.projectcalico.org/manifests/calico.yaml
```



#### 安装node节点
将脚本文件init_node.sh上传到工作节点服务器上，根据需要修改脚本前几行的主机名和节点服务器的IP地址  
然后运行脚本  
```c
chmod +x init_node.sh
./init_node.sh
```

在管理节点master上运行如下命令，获取添加工作节点命令
```c
kubeadm token create --print-join-command
```

将获取到的命令，在node上运行
```c
kubeadm join 192.168.202.128:6443 --token v36lcn.ix6mx6o13c7mv5pr     --discovery-token-ca-cert-hash sha256:67d71bd00c1e292c52b17b2dc10b7c2a6e00d63a7f6c24d419ad583513c62a25
```

注意：管理节点每24小时生成的值不同


## 第二部分（作者：Qist）
下载脚本文件k8s-install.sh，根据需要修改脚本中主机名称和IP地址。

注意：提前将其他服务器的主机名进行修改，和脚本文件中的主机名保持一致，然后再任意节点上安装即可。

