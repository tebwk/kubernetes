hostnamectl set-hostname k8s-master
cat >>/etc/hosts<<EOF
192.168.202.128 kmaster.example.com k8s-master
192.168.202.129 knode1.example.com k8s-node1
192.168.202.130 knode2.example.com k8s-node2
192.168.202.131 knode3.example.com k8s-node3
EOF
systemctl disable firewalld && systemctl stop firewalld
setenforce 0
sed -i --follow-symlinks 's/^SELINUX=enforcing/SELINUX=disabled/' /etc/sysconfig/selinux
sed -i '/swap/d' /etc/fstab
swapoff -a
yum install -y ntpdate
ntpdate time.windows.com
cat >>/etc/sysctl.d/kubernetes.conf<<EOF
net.bridge.bridge-nf-call-ip6tables = 1
net.bridge.bridge-nf-call-iptables = 1
EOF
sysctl --system
cat >>/etc/yum.repos.d/kubernetes.repo<<EOF
[kubernetes]
name=Kubernetes
baseurl=https://mirrors.aliyun.com/kubernetes/yum/repos/kubernetes-el7-x86_64/
enabled=1
gpgcheck=1
repo_gpgcheck=1
gpgkey=https://mirrors.aliyun.com/kubernetes/yum/doc/yum-key.gpg
        https://mirrors.aliyun.com/kubernetes/yum/doc/rpm-package-key.gpg
EOF
yum clean all
yum makecache -y
yum install -y kubeadm kubelet kubectl
systemctl enable kubelet
systemctl start kubelet
sudo yum install -y yum-utils device-mapper-persistent-data lvm2
yum-config-manager --add-repo http://mirrors.aliyun.com/docker-ce/linux/centos/docker-ce.repo
yum install docker-ce docker-ce-cli containerd.io -y
systemctl enable docker
systemctl start docker

