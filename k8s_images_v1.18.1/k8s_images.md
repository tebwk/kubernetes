<!--
 * @Author: e4glet
 * @Date: 2020-05-04 15:56:49
 * @LastEditTime: 2020-05-09 23:22:49
 * @LastEditors: Please set LastEditors
 * @Description: In User Settings Edit
 * @FilePath: \kubernetes\k8s_images_v1.18.1\k8s_images.md
 -->

# kubernetes images list

version v1.18.1

```c
[root@k8s-master ~]# docker images
REPOSITORY                           TAG                 IMAGE ID            CREATED             SIZE
k8s.gcr.io/kube-proxy                v1.18.1             4e68534e24f6        3 weeks ago         117MB
k8s.gcr.io/kube-apiserver            v1.18.1             a595af0107f9        3 weeks ago         173MB
k8s.gcr.io/kube-controller-manager   v1.18.1             d1ccdd18e6ed        3 weeks ago         162MB
k8s.gcr.io/kube-scheduler            v1.18.1             6c9320041a7b        3 weeks ago         95.3MB
k8s.gcr.io/pause                     3.2                 80d28bedfe5d        2 months ago        683kB
k8s.gcr.io/coredns                   1.6.7               67da37a9a360        3 months ago        43.8MB
calico/node                          v3.11.2             81f501755bb9        3 months ago        255MB
calico/cni                           v3.11.2             c317181e3b59        3 months ago        204MB
calico/pod2daemon-flexvol            v3.11.2             f69bca7e2325        3 months ago        111MB
calico/kube-controllers              v3.11.2             9e897df2f2af        3 months ago        52.5MB
k8s.gcr.io/etcd                      3.4.3-0             303ce5db0e90        6 months ago        288MB
```


#### 镜像获取

百度网盘  
链接：https://pan.baidu.com/s/1RkCZJDSKpwoL_eM0_lg2MQ   
提取码：qubl  

#### 加载方法
将文件上传到指定目录,使用如下命令：  

```c
docker load < k8s_images_v1.18.1.tar
```