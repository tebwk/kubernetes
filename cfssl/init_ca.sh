#!/bin/bash

###
 # @Author: e4glet
 # @Date: 2020-05-10 17:46:30
 # @LastEditTime: 2020-05-10 18:31:38
 # @LastEditors: Please set LastEditors
 # @Description: In User Settings Edit
 # @FilePath: \kubernetes\cfssl\init_ca.sh
 ###

echo "下载cfssl工具"
curl -L https://pkg.cfssl.org/R1.2/cfssl_linux-amd64 -o /usr/local/bin/cfssl
curl -L https://pkg.cfssl.org/R1.2/cfssljson_linux-amd64 -o /usr/local/bin/cfssljson
curl -L https://pkg.cfssl.org/R1.2/cfssl-certinfo_linux-amd64 -o /usr/local/bin/cfssl-certinfo
echo "授权cfssl操作权限"
chmod +x /usr/local/bin/cfssl*
echo "开始创建根证书CA"
echo "1.创建根证书CA配置文件 ca-config.json"
cat>ca-config.json<<EOF
{
    "signing": {
      "default": {
        "expiry": "87600h"
    },
    "profiles": {
      "kubernetes": {
        "usages": [
            "signing",
            "key encipherment",
            "server auth",
            "client auth"
        ],
        "expiry": "87600h"
      }
    }
  }
}
EOF
echo "2.创建证书签名请求文件 ca-csr.json"
cat > ca-csr.json << EOF
{
  "CN": "kubernetes",
  "key": {
    "algo": "rsa",
    "size": 2048
  },
  "names": [
    {
      "C": "CN",
      "ST": "Tianjin",
      "L": "Tianjin",
      "O": "k8s",
      "OU": "System"
    }
  ]
}
EOF
echo "生成CA根证书"
cfssl gencert -initca ca-csr.json | cfssljson -bare ca
echo "3.创建 kubernetes 证书签名请求 kubernetes-csr.json"
cat > kubernetes-csr.json << EOF
{
    "CN": "kubernetes",
    "hosts": [
      "127.0.0.1",
      "192.168.202.128",
      "192.168.202.129",
      "10.254.0.1",
      "kubernetes",
      "kubernetes.default",
      "kubernetes.default.svc",
      "kubernetes.default.svc.cluster",
      "kubernetes.default.svc.cluster.local"
    ],
    "key": {
        "algo": "rsa",
        "size": 2048
    },
    "names": [
        {
            "C": "CN",
            "ST": "Tianjin",
            "L": "Tianjin",
            "O": "k8s",
            "OU": "System"
        }
    ]
}
EOF
echo "生成证书及私钥"
cfssl gencert -ca=ca.pem -ca-key=ca-key.pem -config=ca-config.json -profile=kubernetes kubernetes-csr.json | cfssljson -bare kubernetes
echo "4.创建 admin 证书签名请求 admin-csr.json"
cat > admin-csr.json << EOF
{
  "CN": "admin",
  "hosts": [],
  "key": {
    "algo": "rsa",
    "size": 2048
  },
  "names": [
    {
      "C": "CN",
      "ST": "Tianjin",
      "L": "Tianjin",
      "O": "system:masters",
      "OU": "System"
    }
  ]
}
EOF
echo "生成证书及私钥"
cfssl gencert -ca=ca.pem -ca-key=ca-key.pem -config=ca-config.json -profile=kubernetes admin-csr.json | cfssljson -bare admin
echo "5.创建 kube-proxy 证书签名请求 kube-proxy-csr.json"
cat > kube-proxy-csr.json << EOF
{
  "CN": "system:kube-proxy",
  "hosts": [],
  "key": {
    "algo": "rsa",
    "size": 2048
  },
  "names": [
    {
      "C": "CN",
      "ST": "Tianjin",
      "L": "Tianjin",
      "O": "k8s",
      "OU": "System"
    }
  ]
}
EOF
echo "生成证书及私钥"
cfssl gencert -ca=ca.pem -ca-key=ca-key.pem -config=ca-config.json -profile=kubernetes  kube-proxy-csr.json | cfssljson -bare kube-proxy
echo "打包证书到 /k8s/ssl 目录"
mkdir -p /k8s/ssl
cp *.pem /k8s/ssl
cd /k8s/ssl
ls
echo "打包证书完成"
