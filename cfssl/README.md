<!--
 * @Author: e4glet
 * @Date: 2020-05-10 17:37:23
 * @LastEditTime: 2020-05-10 18:32:29
 * @LastEditors: Please set LastEditors
 * @Description: In User Settings Edit
 * @FilePath: \kubernetes\cfssl\cfssl.md
 -->

# 使用cfssl自签证书用于Kubernetes


#### 自签证书一键生成脚本

```c
chmod +x init_ca.sh
./init_ca.sh
```

#### 创建根证书CA配置文件  

```c
cat > ca-config.json <<EOF
{
  "signing": {
    "default": {
      "expiry": "8760h"
    },
    "profiles": {
      "kubernetes": {
        "usages": [
            "signing",
            "key encipherment",
            "server auth",
            "client auth"
        ],
        "expiry": "8760h"
      }
    }
  }
}
EOF
```

###
ca-config.json：可以定义多个 profiles，分别指定不同的过期时间、使用场景等参数；后续在签名证书时使用某个 profile；此实例只有一个kubernetes模板。
signing：表示该证书可用于签名其它证书；生成的 ca.pem 证书中 CA=TRUE；
server auth：表示client可以用该 CA 对server提供的证书进行验证；
client auth：表示server可以用该CA对client提供的证书进行验证；
###

#### 创建证书签名请求文件  

```c
cat > ca-csr.json << EOF
{
    "CN": "etcd CA",
    "key": {
        "algo": "rsa",
        "size": 2048
    },
    "names": [
        {
            "C": "CN",
            "L": "Tianjin",
            "ST": "Tianjin"
        }
    ]
}
EOF
```

生成CA根证书  
```c
cfssl gencert -initca ca-csr.json | cfssljson -bare ca
```

#### 创建 kubernetes 证书签名请求  

```c
cat > kubernetes-csr.json << EOF
{
    "CN": "kubernetes",
    "hosts": [
      "127.0.0.1",
      "192.168.202.128",
      "192.168.202.129",
      "10.254.0.1",
      "kubernetes",
      "kubernetes.default",
      "kubernetes.default.svc",
      "kubernetes.default.svc.cluster",
      "kubernetes.default.svc.cluster.local"
    ],
    "key": {
        "algo": "rsa",
        "size": 2048
    },
    "names": [
        {
            "C": "CN",
            "ST": "Tianjin",
            "L": "Tianjin",
            "O": "k8s",
            "OU": "System"
        }
    ]
}
EOF
```
生成证书及私钥  
```c
cfssl gencert -ca=ca.pem -ca-key=ca-key.pem -config=ca-config.json -profile=kubernetes kubernetes-csr.json | cfssljson -bare kubernetes
```

#### 创建 admin 证书签名请求  

```c
cat > admin-csr.json << EOF
{
    "CN": "admin",
    "hosts": [],
    "key": {
      "algo": "rsa",
      "size": 2048
    },
    "names": [
      {
        "C": "CN",
        "ST": "Tianjin",
        "L": "Tianjin",
        "O": "system:masters",
        "OU": "System"
      }
    ]
}
EOF
```
生成 admin 证书和私钥  
```c
cfssl gencert -ca=ca.pem -ca-key=ca-key.pem -config=ca-config.json -profile=kubernetes admin-csr.json | cfssljson -bare admin
```

#### 创建 kube-proxy 证书签名请求  

```c
cat > kube-proxy-csr.json << EOF
{
    "CN": "system:kube-proxy",
    "hosts": [],
    "key": {
      "algo": "rsa",
      "size": 2048
    },
    "names": [
      {
        "C": "CN",
        "ST": "Tianjin",
        "L": "Tianjin",
        "O": "k8s",
        "OU": "System"
      }
    ]
}
EOF
```

生成 kube-proxy 客户端证书和私钥   
```c
 cfssl gencert -ca=ca.pem -ca-key=ca-key.pem -config=ca-config.json -profile=kubernetes  kube-proxy-csr.json | cfssljson -bare kube-proxy
```