<!--
 * @Author: e4glet
 * @Date: 2020-05-10 21:31:33
 * @LastEditTime: 2020-06-30 10:46:20
 * @LastEditors: Please set LastEditors
 * @Description: In User Settings Edit
 * @FilePath: \kubernetes\NFS\nfs.md
 -->

# 使用loopfs创建加密磁盘镜像并挂载到kubernetes应用部署微服务

#### loopfs配置
参考资料：  
https://www.thegeekdiary.com/how-to-create-virtual-block-device-loop-device-filesystem-in-linux/

#### 配置全过程

请查阅 [使用loopfs创建加密磁盘镜像并挂载到kubernetes应用部署微服务](https://gitee.com/e4glet/kubernetes/blob/master/docs/使用loopfs创建加密磁盘镜像并挂载到kubernetes应用部署微服务.pdf)

#### 测试与效验
创建测试pod  
[root@k8s-master ~]# vi test-storage.yaml  

```c
apiVersion: apps/v1
kind: Deployment
metadata:
  name: mytomcat
spec:
  replicas: 5
  selector:
    matchLabels:
      app: mytomcat
  minReadySeconds: 1
  progressDeadlineSeconds: 60
  revisionHistoryLimit: 5
  strategy:
    type: RollingUpdate
    rollingUpdate:
      maxSurge: 1
      maxUnavailable: 1
  template:
    metadata:
      name: mytomcat
      labels:
        app: mytomcat
    spec:
      containers:
      - name: mytomcat
        image: tomcat:8
        ports:
        - containerPort: 8080        
      storageClassName: storage-img     
      storage-img:
        path: /loopfs
        server: 192.168.202.130
```


#### 加密操作方法
```c
cd /home/user
mkdir loop #创建空目录
dd if=/dev/zero of=/home/user/loop.img bs=1M count=4096 # 生成映像文件
losetup /dev/loop0 /home/user/loop.img # 创建loop设备,在/dev下会生成一个loop0块设备
fdisk /dev/loop0 # 分区 
partprobe /dev/loop0
cryptsetup luksFormat /dev/loop0p1 # 加密
cryptsetup open /dev/loop0p1 loop0p1 # 加密的分区映射
mkfs.ext4 /dev/mapper/loop0p1 # 格式化加密分区
mount /dev/mapper/loop0p1 /home/user/loop  # mount加密分区
```